import { HelloController } from "./controller/hello.controller";
import { TestController } from "./controller/test.controller";
import bodyParser from "body-parser";
import express from "express";

export function createApp() {
  const helloController = new HelloController();
  const testController = new TestController();

  const app = express();
  app.use(bodyParser.json());
  app.use("/", helloController.getRouter());
  app.use("/test", testController.getRouter());
  return app;
}
