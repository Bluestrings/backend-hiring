import mongoose from "mongoose";

export interface ITest extends mongoose.Document {
  patientName: string;
  patientAge: Number;
  testOutcome: Boolean;
  location: String;
  country: String;
  timestamp: Number;
}

export const TestSchema = new mongoose.Schema({
  patientName: String,
  patientAge: Number,
  testOutcome: Boolean,
  location: String,
  country: String,
  timestamp: Number
});

const Test = mongoose.model<ITest>("Test", TestSchema);
export default Test;
