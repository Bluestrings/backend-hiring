import { Request, Response, Router } from "express";
import axios from "axios";
import Test from "../model/dbTest"

export class TestController {
  getRouter(): Router {
    const router = Router();
    router.post("/", this.uploadTest.bind(this));
    router.get("/", this.getTest.bind(this));
    router.get("/:day/:month/:year", this.dailyTests.bind(this));
    router.get("/positive/:day/:month/:year", this.dailyPositiveTests.bind(this));
    router.get("/:country/:day/:month/:year", this.countryPositiveTests.bind(this));
    router.get("/debugDelete/:docId", this.deleteDoc.bind(this));
    router.get("/debugClear", this.clearDB.bind(this));
    return router;
  }

  async uploadTest(request: Request, response: Response) {
    let countryLookup = await axios.get(`https://api.opencagedata.com/geocode/v1/json?q=${request.body.coords}&key=2036eb728ced4512b3f938a45b3ed400`)
    // console.log(countryLookup.data.results[0].components.country);
    let country = countryLookup.data.results[0].components.country
    let newTest = new Test({
        patientName: request.body.patientName,
        patientAge: request.body.patientAge,
        testOutcome: request.body.testOutcome,
        location: request.body.coords,
        country,
        timestamp: Date.now()
    });
    await newTest.save(function (err, doc) {
        if (!err) {
            // console.log('New Upload: ', doc);
            return response.json(doc).status(200);
        } else {
            // console.log("Failed upload: ", err);
            return response.json(err).status(400);
        }
    });
  }

  async getTest(request: Request, response: Response) {
    let tests = await Test.find();
    // console.log(tests);
    return response.json(tests).status(200);
  }

  async clearDB(request: Request, response: Response) {
    await Test.deleteMany({ patientName: 'test' });
    // console.log(await Test.find());
    return response.send("Test Entries deleted").status(200);
  }

  async deleteDoc(request: Request, response: Response) {
    await Test.deleteMany({ _id: request.params.docId });
    // console.log(await Test.find());
    return response.send("Document Deleted").status(200);
  }

  async dailyTests(request: Request, response: Response) {
    let d = new Date(`${request.params.year}-${request.params.month}-${request.params.day}T00:00:00Z`)
    let d2 = new Date(`${request.params.year}-${request.params.month}-${request.params.day}T23:59:59Z`)
    console.log(d)
    console.log(d2)
    let dayTests = await Test.find({ timestamp: { $gt: d.getTime(), $lt: d2.getTime()} });
    // console.log(dayTests);
    return response.json(dayTests).status(200);
  }

  async dailyPositiveTests(request: Request, response: Response) {
    let d = new Date(`${request.params.year}-${request.params.month}-${request.params.day}T00:00:00Z`);
    let d2 = new Date(`${request.params.year}-${request.params.month}-${request.params.day}T23:59:59Z`);
    let dayTests = await Test.find({ timestamp: { $gt: d.getTime(), $lt: d2.getTime()}, testOutcome: true });
    // console.log(dayTests);
    return response.json(dayTests).status(200);
  }

  async countryPositiveTests(request: Request, response: Response) {
    let d = new Date(`${request.params.year}-${request.params.month}-${request.params.day}T00:00:00Z`);
    let d2 = new Date(`${request.params.year}-${request.params.month}-${request.params.day}T23:59:59Z`);
    let dayTests = await Test.find({ timestamp: { $gt: d.getTime(), $lt: d2.getTime()}, country: request.params.country });
    // console.log(dayTests);
    return response.json(dayTests).status(200);
  }
}
