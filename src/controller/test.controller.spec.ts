import request from "supertest";
import assert from "assert";
import { connect } from "mongoose";
import { createApp } from "../app";
const app = createApp();

connect("mongodb://treeline:treeline@localhost:27017/jshiring", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

var docId = '';
describe("TestController", () => {
  it("returns 200 on GET /test", async (done) => {
    let test = await request(app).get("/test");
    assert.strictEqual(test.status, 200);
    done();
  });

  it("returns 200 + test data on POST /test", async (done) => {
    let test = await request(createApp()).post("/test").send({
        "patientName": "test",
        "patientAge": 10,
        "testOutcome": true,
        "coords": "52.3877830,9.7334394"
    });
    assert.strictEqual(test.status, 200);
    assert.strictEqual(test.body.country, "Germany");
    docId = test.body._id;
    done();
  });

  it("returns 200 + test data on GET /test/:day/:month/:year", async (done) => {
    let time = new Date(Date.now());
    var day = time.getDate() < 10 ? '0' + time.getDate() : time.getDate()
    var month = time.getMonth() + 1 < 10 ? '0' + (time.getMonth() + 1) : time.getMonth() + 1
    let test = await request(createApp()).get(`/test/${day}/${month}/${time.getFullYear()}`);
    assert.strictEqual(test.status, 200);
    done();
  });

  it("returns 200 + test data on GET /:country/:day/:month/:year", async (done) => {
    let time = new Date(Date.now());
    var day = time.getDate() < 10 ? '0' + time.getDate() : time.getDate()
    var month = time.getMonth() + 1 < 10 ? '0' + (time.getMonth() + 1) : time.getMonth() + 1
    let test = await request(createApp()).get(`/test/Germany/${day}/${month}/${time.getFullYear()}`);
    assert.strictEqual(test.status, 200);
    for(const entry in test.body) {
        assert.strictEqual(test.body[entry].country, "Germany");
    }
    done();
  });

  it("returns 200 + test data on GET /positive/:day/:month/:year", async (done) => {
    let time = new Date(Date.now());
    var day = time.getDate() < 10 ? '0' + time.getDate() : time.getDate()
    var month = time.getMonth() + 1 < 10 ? '0' + (time.getMonth() + 1) : time.getMonth() + 1
    let test = await request(createApp()).get(`/test/positive/${day}/${month}/${time.getFullYear()}`);
    assert.strictEqual(test.status, 200);
    for(const entry in test.body) {
        assert.strictEqual(test.body[entry].testOutcome, true);
    }
    done();
  });
});
